<!doctype html>
<html lang="es">
  <head>
    <title>Colorlib Breed</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700,800" rel="stylesheet">

    <link rel="stylesheet" href="{{asset("assets/css/bootstrap.css")}}">
    <link rel="stylesheet" href="{{asset("assets/css/animate.css")}}">
    <link rel="stylesheet" href="{{asset("assets/css/owl.carousel.min.css")}}">

    <link rel="stylesheet" href="{{asset("assets/onts/ionicons/css/ionicons.min.css")}}">
    <link rel="stylesheet" href="{{asset("assets/fonts/fontawesome/css/font-awesome.min.css")}}">

    <link rel="stylesheet" href="{{asset("assets/fonts/flaticon/font/flaticon.css")}}">

    @yield('styles')

    <!-- Theme Style -->
    <link rel="stylesheet" href="{{asset("assets/css/style.css")}}">
  </head>
  <body>

      <!-- Inicio Header -->
      @include("theme/$theme/header")
      <!-- Fin Header -->


    <section class="content">

        @yield('contenido')

    </section>



        <!-- Inicio Footer -->
        @include("theme/$theme/footer")
        <!-- Fin Footer -->


  </body>
</html>
